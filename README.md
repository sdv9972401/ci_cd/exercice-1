# 🚴 HugoVelo - Site statique dans un container Docker

![](screenshot.png)

<!-- TOC -->
* [🚴 HugoVelo - Site statique dans un container Docker](#-hugovelo---site-statique-dans-un-container-docker)
  * [🛠️Utilisation](#utilisation)
    * [🌍Accès au site](#accès-au-site)
  * [📝 Modification du contenu](#-modification-du-contenu)
    * [🖋 Hugo et Markdown](#-hugo-et-markdown)
<!-- TOC -->

Ce repo contient le code source d'un site web statique généré à l'aide du framework [Hugo](https://gohugo.io/).

Le `Dockerfile` fourni permet le build d'une image permettant de tester le bon fonctionnement du site web, en utilisant
le server web de développement fourni avec Hugo.

## 🐳 Description du Dockerfile étape par étape

1. 🏗️ **Image de base**:
   - `FROM alpine:latest`
   - Utilise la dernière version de l'image Alpine Linux comme base.

2. 📁 **Définition du répertoire de travail**:
    - `WORKDIR app/`
    - Définit le répertoire de travail à `/app/`.

3. 📦 **Installer Hugo et Git**:
   ```Dockerfile
     RUN apk add --no-cache --repository=https://dl-cdn.alpinelinux.org/alpine/edge/community hugo && \
         apk add --no-cache git
     ```
    - Installe Hugo depuis le référentiel de la communauté Alpine et Git.

4. 📄 **Copie des sources vers le container**:
    - `COPY . .`
    - Copie le contenu du répertoire local dans le répertoire de travail du conteneur.

5. 📦 **Récupération du thème**:
   ```Dockerfile
     RUN git submodule add --force https://github.com/theNewDynamic/gohugo-theme-ananke.git velohugo/themes/ananke
     ```
    - Clone le thème depuis le référentiel Git spécifié et l'ajoute comme sous-module git dans le chemin indiqué.

6. 🌐 **Exposer le port 8080**:
    - `EXPOSE 8080`
    - Expose le port 8080 pour permettre la communication avec l'extérieur.

7. 📂 **Changement du répertoire de travail**:
    - `WORKDIR /app/velohugo`
    - Change le répertoire de travail vers `/app/velohugo` en préparation de la commande ci-dessous.

8. 🚀 **Start development server**:
    - `CMD ["hugo", "server", "--bind=0.0.0.0", "--port=8080"]`
    - Démarre le serveur de développement Hugo, en écoutant sur le port 8080.

## 🛠️Utilisation

- En premier lieu, cloner le repo : 

```bash
git clone https://gitlab.com/sdv9972401/ci_cd/exercice-1
```

- Construire l'image Docker :

```bash
cd exercice-1 && docker build -t fab/hugovelo .
```

- Lancer le container en montant un volume contenant les articles :
```bash
docker run -p 8080:8080 -v "${PWD}/velohugo/content:/app/velohugo/content" fab/hugovelo
```

>ℹ️Le répertoire `content` situé à la racine du dossier `velo-hugo` contient les fichiers sources en 
>`Markdown` utilisé par Hugo pour générer les pages web. En montant les données contenues dans le volume du container, on est ainsi
>en mesure de modifier le contenu du site web au besoin.

### 🌍Accès au site

Ouvrir un navigateur et accéder à http://localhost:8080 pour voir le site en développement.

## 📝 Modification du contenu

Il est possible modifier le contenu du site en éditant les fichiers dans le répertoire local spécifié lors du montage du volume (`content` dans l'exemple ci-dessus). 
Les modifications seront reflétées automatiquement dans le site en cours d'exécution.

### 🖋 Hugo et Markdown

L'exemple ci-dessous permet de tester la génération automatique des pages web en modifiant le contenu du dossier monté
dans le container.

L'arborescence du dossier `content` est définie comme ceci : 

```
content
├── _index.md
└── posts
    ├── _index.md
    └── article1.md
```

Pour ajouter un nouveau post, on crée un nouveau fichier nommé ici `article2.md` dans le dossier `content/posts/`.

```bash
touch content/posts/article2.md
```

En début de fichier, il faut définir le font-matter afin que Hugo puisse correctement générer la page : 

```yaml
---
date: 2024-01-02T10:58:08-04:00
description: "Mon article de test"
featured_image: "/images/image.jpg"
tags: ["test"]
title: "Article de test"
---

# Titre 1
Mon texte ici
```

Enregistrer le fichier. Le nouvel article devrais apparaître.

ℹ️ **Remarques**

> L'image expose le port 8080, bien vérifier qu'aucun port n'écoute sur 8080.
> Le site Hugo est accessible à http://localhost:8080 une fois le conteneur en cours d'exécution.