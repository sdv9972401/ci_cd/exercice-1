# Base image
FROM alpine:latest

# Setup working directory
WORKDIR app/

# Install Hugo and Git
RUN apk add --no-cache --repository=https://dl-cdn.alpinelinux.org/alpine/edge/community hugo && \
    apk add --no-cache git

# Copy website
COPY . .

# Clone the theme
RUN git submodule add --force https://github.com/theNewDynamic/gohugo-theme-ananke.git velohugo/themes/ananke

# Expose port 8080
EXPOSE 8080

# Change directory
WORKDIR /app/velohugo

# Start development server
CMD ["hugo", "server", "--bind=0.0.0.0", "--port=8080"]